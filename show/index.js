const h1 = document.createElement('h1')
document.body.appendChild(h1)


const updatePeers = peers => {
  h1.innerHTML = `peers: ${peers}`
  document.title = `peers: ${peers}`
}

const check = async () => {
  const peersResult = await fetch('peers.txt')
  const peers = await peersResult.text()
  updatePeers(peers)
  setTimeout(check, 500)
}

check()
