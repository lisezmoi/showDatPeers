# showDatPeers

A P2P web page that shows its dat peers.

![Screenshot](screenshot.png)

## Usage

`npm install`

`ǹpm start`

Then visit the generated `dat://` link using a dat compatible browser such as [Beaker](http://beakerbrowser.com/).

## Authors

[Pierre Bertet](https://pierre.world/) and [Raphaël Bastide](http://raphaelbastide.com/)

## License

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
