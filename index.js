const fs = require('fs')
const path = require('path')
const Dat = require('dat-node')

const sharedDir = path.join(__dirname, 'show')

const getPeersCount = stats =>
  stats.peers && stats.peers.total ? stats.peers.total : 0

const updatePeersFile = (peersCount, dat, cb) => {
  fs.writeFile(`${sharedDir}/peers.txt`, peersCount, err => {
    if (err) throw err
    dat.importFiles()
    if (cb) cb()
  })
}

const checkPeers = (stats, dat, cb) => {
  const peersCount = getPeersCount(stats)
  if (peersCount === lastPeersCount) {
    if (cb) cb()
    return
  }

  lastPeersCount = peersCount
  updatePeersFile(peersCount, dat, cb)
  console.log('Peers count:', peersCount)
}

let lastPeersCount = -1

Dat(
  sharedDir,

  (err, dat) => {
    if (err) throw err

    dat.importFiles()

    const network = dat.joinNetwork()

    const stats = dat.trackStats()

    console.log('')
    console.log(`Dat link: dat://${dat.key.toString('hex')}`)
    console.log('')

    const check = () => {
      checkPeers(stats, dat, () => {
        setTimeout(check, 1000)
      })
    }

    check()
  }
)
